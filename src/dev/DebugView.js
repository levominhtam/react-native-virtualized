import React from "react"
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Platform,
  TouchableOpacity
} from "react-native"
import Animated from "react-native-reanimated"
import { PanGestureHandler, State } from "react-native-gesture-handler"
const { width, height } = Dimensions.get("window")

const {
  block,
  cond,
  eq,
  not,
  add,
  sub,
  set,
  multiply,
  Value,
  event,
  interpolate,
  Extrapolate
} = Animated

const MAX_LOGS = 7
const TAG = "DebugView"

export default class DebugView extends React.PureComponent {
  dragX = new Value(0)
  dragY = new Value(0)
  offsetX = new Value(0)
  offsetY = new Value(0)
  hidden = new Value(1)
  isHidden = true
  width = new Value(0)
  height = new Value(0)
  gestureState = new Value(-1)
  onGestureEvent = event([
    {
      nativeEvent: {
        translationX: this.dragX,
        translationY: this.dragY,
        state: this.gestureState
      }
    }
  ])
  transX = block([
    cond(this.hidden, multiply(sub(this.width, 40), -1), cond(
      eq(this.gestureState, State.ACTIVE),
      add(this.offsetX, this.dragX),
      set(this.offsetX, add(this.offsetX, this.dragX))
    )),
  ])
  transY = block([
    cond(this.hidden, multiply(sub(this.height, 40), 1), cond(
      eq(this.gestureState, State.ACTIVE),
      add(this.offsetY, this.dragY),
      set(this.offsetY, add(this.offsetY, this.dragY))
    ))
  ])

  state = {
    logs: Array.from(Array(MAX_LOGS), () => ""),
    cellUnused: 0,
    cellTotal: 0,
    itemTotal: 0,
    scaleX: 1,
    scaleY: 1
  }

  static instance: DebugView
  static addLog(message) {
    this.instance && this.instance.addLog(message)
  }

  static setCellInfo(options) {
    this.instance && this.instance.setCellInfo(options)
  }

  static setItemInfo(options) {
    this.instance && this.instance.setItemInfo(options)
  }

  static setViewport(options) {
    this.instance && this.instance.setViewport(options)
  }

  static setScale(options) {
    this.instance && this.instance.setScale(options)
  }

  addLog(message) {
    const { logs } = this.state
    const next = [...logs]
    next.push(message)
    if (next.length > MAX_LOGS) {
      next.splice(0, 1)
    }
    this.setState({ logs: next })
  }

  setCellInfo({ cellUnused, cellTotal }) {
    this.setState({ cellUnused, cellTotal })
  }

  setItemInfo({ itemTotal }) {
    this.setState({ itemTotal })
  }

  setViewport(viewportText) {
    this.setState({ viewport: viewportText })
  }

  setScale({ x: scaleX, y: scaleY }) {
    this.setState({ scaleX, scaleY })
  }

  onLayout = ({ nativeEvent: { layout } }) => {
    this.width.setValue(layout.width)
    this.height.setValue(layout.height)
  }

  render() {
    const {
      cellTotal,
      cellUnused,
      itemTotal,
      viewport,
      scaleX,
      scaleY,
      logs
    } = this.state
    // console.log(TAG, 'render')
    return (
      <PanGestureHandler
        maxPointers={1}
        onGestureEvent={this.onGestureEvent}
        onHandlerStateChange={this.onGestureEvent}
      >
        <Animated.View
          onLayout={this.onLayout}
          style={[
            styles.container,
            this.props.style,
            {
              position: "absolute",
              left: 0,
              bottom: 0,
              width: (width * 2) / 3,
              backgroundColor: "black",
              borderColor: "dimgray",
              borderWidth: 2,
              paddingVertical: 8,
              paddingHorizontal: 16,
              transform: [
                { translateX: this.transX },
                { translateY: this.transY }
              ]
            }
          ]}
        >
          {/* CELL POOL */}
          <Text style={{ fontSize: 10, color: "whitesmoke" }}>
            Free cells: {cellUnused}/{cellTotal}
          </Text>

          <Text style={{ fontSize: 10, color: "whitesmoke", marginTop: 4 }}>
            On-screen items: {cellTotal - cellUnused}/{itemTotal}
          </Text>

          <Text style={{ fontSize: 10, color: "whitesmoke", marginTop: 4 }}>
            Viewport: {viewport}
          </Text>

          <Text style={{ fontSize: 10, color: "whitesmoke", marginTop: 4 }}>
            Scale: x:{Number(scaleX).toFixed(2)}, y:{Number(scaleY).toFixed(2)}
          </Text>

          <Text style={{ fontSize: 10, color: "whitesmoke", marginTop: 4 }}>
            Console{"\n"}
            -------{"\n"}
            <Text
              style={{
                fontSize: 10,
                fontFamily: Platform.select({
                  ios: "Menlo",
                  android: "monospace"
                })
              }}
            >
              {this.state.logs.join("\n")}
            </Text>
          </Text>

          {this.props.children}

          {/* HIDE/UN-HIDE button */}
          <TouchableOpacity
            onPress={() => {
              this.isHidden = !this.isHidden
              this.hidden.setValue(this.isHidden)
            }}
            style={{
              position: "absolute",
              right: 0,
              top: 0,
              width: 32,
              height: 32,
              backgroundColor: "transparent",
              borderRadius: 4,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text style={{ fontSize: 21, fontWeight: "bold", color: "white" }}>
              〄
            </Text>
          </TouchableOpacity>
        </Animated.View>
      </PanGestureHandler>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    zIndex: 999
  },
  box: {
    flex: 1,
    backgroundColor: "tomato"
  }
})
