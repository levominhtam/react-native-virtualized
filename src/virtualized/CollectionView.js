import { SectionOverlay } from "./debug/SectionOverlay"
import { ViewportView } from "./debug/ViewportView"
import { OverscanView } from "./debug/OverscanView"
import React, { Component, PureComponent } from "react"
import {
  Dimensions,
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  Platform,
  ToastAndroid,
} from "react-native"
import {
  PanGestureHandler,
  State,
  PinchGestureHandler,
  RectButton,
} from "react-native-gesture-handler"
import Animated, { Easing } from "react-native-reanimated"
import _ from "lodash"
import Batchinator from "react-native/Libraries/Interaction/Batchinator"
import CellRender from "./CellRender"
import DebugView from "../dev/DebugView"
const DebugSectionBackgroundImage = require("../assets/rect-50x50-without-filled.png")
const uuidv4 = require("uuid/v4")

const {
  concat,
  set,
  cond,
  eq,
  or,
  add,
  sub,
  pow,
  min,
  max,
  debug,
  multiply,
  divide,
  lessThan,
  spring,
  defined,
  decay,
  timing,
  call,
  diff,
  acc,
  not,
  abs,
  block,
  startClock,
  stopClock,
  clockRunning,
  onChange,
  Value,
  Clock,
  event
} = Animated

const MIN_SCALE = 1
const MAX_SCALE = 4

const IMAGE_WIDTH = 200
const IMAGE_HEIGHT = 200

const POOL_STEP = 5
const POOL_SIZE_MIN = 20
const POOL_SIZE_MAX = 60

const TAG = "CollectionView"

class CollectionView extends Component {
  static defaultProps = {
    horizontalOverscanSize: 25,
    verticalOverscanSize: 25,
    updateCellsBatchingPeriod: 16, // ms
    minScale: 1,
    maxScale: 4,
    debug: false
  }

  __stack = []

  constructor(props) {
    super(props)
    const {
      layoutManager,
      updateCellsBatchingPeriod,
      addScrollListener,
      scrollContext
    } = props

    this.prepareLayoutIfNeed()

    const { contentWidth, contentHeight } = scrollContext
    const { width, height } = layoutManager.getContentSize()
    contentWidth.setValue(width)
    contentHeight.setValue(height)

    this._updateCellsToRenderBatcher = new Batchinator(
      this._updateCellsToRender,
      updateCellsBatchingPeriod
    )

    this._lastRenderedCellIndices = []
    this._unusedCells = []
    this._cellBinding = new Map()
    this.appendUnusedCells(POOL_SIZE_MIN)

    this._viewportMetric = { left: 0, top: 0, right: 0, bottom: 0 }
    this._isScrolling = false

    this.state = {
      childrenToDisplay: [],
      indicesToDisplay: []
    }

    addScrollListener('collectionview', this.onScrollChanged)

    /** FOR TESTING ONLY */
    this.__stack = []
  }

  componentDidMount() {
    this._scheduleUpdateCellsToRender()
    if (this.props.debug) {
      setTimeout(() => {
        DebugView.setItemInfo({ itemTotal: this.props.data.length })
      }, 100);
    }
  }

  componentWillUpdate() {
    this._start = Date.now()
  }

  componentDidUpdate() {
    const duration = Date.now() - this._start
    const space = Array.from(Array(duration), (_, d) => (d % 2) === 0 ? "-" : "").join("")
    // console.log('CollectionView', `updated in (ms) ${space} ${duration}`)

    if (this.props.debug) {
      DebugView.addLog(`re-render in ${duration} ms`)
    }
  }

  componentWillUnmount() {
    const { removeScrollListeners } = this.props
    removeScrollListeners()
    this._updateCellsToRenderBatcher.dispose({ abort: true })
  }

  onScrollChanged = (viewport, isScrolling) => {
    const isScrollChanged = this._isScrolling !== isScrolling
    if (isScrollChanged) {
      this.handleScrollChanged(isScrolling)
    }

    const isViewportChanged = !_.isEqual(this._viewportMetric, viewport)
    if (isViewportChanged) {
      this.handleViewportChanged(viewport)
    }
  }

  prepareLayoutIfNeed() {
    const { cellCount, layoutManager } = this.props
    if (
      this._lastRenderedCellCount !== cellCount ||
      this._lastRenderedCellLayoutManager !== layoutManager ||
      this._calculateSizeAndPositionDataOnNextUpdate
    ) {
      this._lastRenderedCellCount = cellCount
      this._lastRenderedCellLayoutManager = layoutManager
      this._calculateSizeAndPositionDataOnNextUpdate = false

      console.log("prepareLayout CALLED")
      layoutManager.prepareLayout()
    }
  }

  handleScrollChanged = scrolling => {
    const { isScrollingChanged, updateCellsBatchingPeriod, layoutManager } = this.props
    this._isScrolling = scrolling

    layoutManager.isScrollingChanged(scrolling)

    isScrollingChanged && isScrollingChanged(scrolling)

    this.setState({ isScrolling: scrolling })

    if (scrolling) {
      if (this._refreshPoolTimer) {
        clearTimeout(this._refreshPoolTimer)
      }
    } else {
      this._refreshPoolTimer = setTimeout(() => {
        this.reduceCellPools()
      }, updateCellsBatchingPeriod * 2) // double waiting time for safety
    }
  }

  reduceCellPools() {
    // if pool size larger than POOL_SIZE_MAX, reduce it to improve memory usage
    const poolSize = this._cellBinding.size
    if (poolSize <= POOL_SIZE_MAX) {
      return
    }

    const freeCells = this._unusedCells.length
    const willDeletedCells = freeCells - POOL_STEP
    if (willDeletedCells < 1) return

    for (let i = 0; i < willDeletedCells; i++) {
      const key = this._unusedCells.pop()
      this._cellBinding.delete(key)
    }
  }

  handleViewportChanged = ({ left, top, right, bottom }) => {
    const {
      layoutManager,
      isScrollingChange,
      horizontalOverscanSize,
      verticalOverscanSize
    } = this.props

    this._viewportMetric.left = left
    this._viewportMetric.top = top
    this._viewportMetric.right = right
    this._viewportMetric.bottom = bottom

    if (this.props.debug) {
      const { left, top, right, bottom } = this._viewportMetric
      DebugView.setViewport(
        `${Math.round(left - horizontalOverscanSize)}, ${Math.round(
          top - verticalOverscanSize
        )}, ${Math.round(right + horizontalOverscanSize)}, ${Math.round(
          bottom + verticalOverscanSize
        )}`
      )
    }

    this._scheduleUpdateCellsToRender()
  }

  _scheduleUpdateCellsToRender = context => {
    this._updateCellsToRenderBatcher.schedule()
  }

  _updateCellsToRender = () => {
    const {
      layoutManager,
      horizontalOverscanSize,
      verticalOverscanSize
    } = this.props
    const { isScrolling } = this.state
    const { left, top, right, bottom } = this._viewportMetric

    const x = left - horizontalOverscanSize
    const y = top - verticalOverscanSize
    const width = right - left + horizontalOverscanSize * 2
    const height = bottom - top + verticalOverscanSize * 2

    const displayes = layoutManager.getCellIndices({
      isScrolling,
      x,
      y,
      width,
      height
    })

    const hasChanged = !_.isEqual(this._lastRenderedCellIndices, displayes)
    if (!hasChanged) {
      // console.log(TAG, "_updateCellsToRender not changed > be skipped.")
      return
    }

    this._updateViewabilityCell(displayes)

    this.setState({
      indicesToDisplay: displayes
    })
  }

  _updateViewabilityCell = displayes => {
    // 1. get all diff (displayed, undisplayed)
    const willBeDisplayed = _.difference(
      displayes,
      this._lastRenderedCellIndices
    )
    const willBeUndisplayed = _.difference(
      this._lastRenderedCellIndices,
      displayes
    )

    // 2. check undisplayed and unlink it with CellRender
    willBeUndisplayed.forEach(this.unlink)
    willBeDisplayed.forEach(this.link)

    // 3. save last indices
    this._lastRenderedCellIndices = displayes

    if (this.props.debug) {
      DebugView.setCellInfo({
        cellTotal: this._cellBinding.size,
        cellUnused: this._unusedCells.length
      })
    }
    // console.log(TAG, 'visible indices:', displayes);
    // console.log(TAG, '  show:', willBeDisplayed);
    // console.log(TAG, '  hide:', willBeUndisplayed);
    // console.log(TAG, '~~~> binding:', Array.from(this._cellBinding));
    // console.log(TAG, '~~~> recycle cells / total:', this._unusedCells.length, '/', this._unusedCellsTotal);
  }

  unlink = indice => {
    const cellKey = this.getCellKey(indice)
    if (cellKey) {
      this._cellBinding.set(cellKey, null)
      this._unusedCells.push(cellKey)
    }
  }

  link = indice => {
    const cellKey = this.getUnusedCellKey()
    this._cellBinding.set(cellKey, indice)
  }

  getCellKey(indice) {
    for (let [key, value] of this._cellBinding) {
      if (indice === value) {
        return key
      }
    }
    return undefined
  }

  getUnusedCellKey() {
    if (this._unusedCells.length === 0) {
      this.appendUnusedCells(POOL_STEP)
    }
    return this._unusedCells.splice(0, 1)[0]
  }

  appendUnusedCells(count) {
    for (let i = 0; i < count; i++) {
      const uuid = uuidv4()
      this._unusedCells.push(uuid)
      this._cellBinding.set(uuid, null)
    }
  }

  /**
  |--------------------------------------------------
  | PUBLIC METHODS
  |--------------------------------------------------
  */
  scrollToIndex = ({index}) => {
    console.log(`scrollToIndex(${index})`)
    const { layoutManager, parent } = this.props
    if (!parent) return
    console.log(`scrollToIndex(${index}) - parent OK`)

    const cellMetadata = layoutManager.getCellMetadata({ index })
    if (!cellMetadata) {
      console.log(`WARN: cellMetadata not found. Cannot scroll to index ${index}`)
      return
    }
    console.log(`scrollToIndex(${index}) - cellMetadata OK`)
    console.log(`scrollToIndex(${index}) - parent.scrollTo OK`)

    parent.scrollTo(cellMetadata)
  }

  /**
  |--------------------------------------------------
  | PUBLIC METHODS - END
  |--------------------------------------------------
  */

  renderCell = indiceIndex => {
    if (indiceIndex == null) {
      return null
    }

    const children = this.props.layoutManager.cellRenderer({
      indice: indiceIndex,
      isScrolling: this.state.isScrolling
    })
    return children
  }

  render() {
    const {
      layoutManager,
      horizontalOverscanSize,
      verticalOverscanSize,
      debug,
      style,
      scrollContext,
    } = this.props
    const { indicesToDisplay, isScrolling } = this.state

    this.prepareLayoutIfNeed()

    const bodyComponents = []
    for (let [key, value] of this._cellBinding) {
      let cellMetadata = layoutManager.getCellMetadata({ index: value })
      let cellConfig = layoutManager.getCellConfig({ index: value })
      if (cellMetadata == null) {
        cellMetadata = {}
      }
      if (cellConfig == null) {
        cellConfig = {}
      }
      bodyComponents.push(
        <CellRender
          key={key}
          index={key}
          boundTo={value}
          cellMetadata={cellMetadata}
          cellConfig={cellConfig}
          context={scrollContext}
          render={this.renderCell}
        />
      )
    }

    return (
        <View style={[styles.container, style]} collapsable={false}>
          {bodyComponents}
        </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})

export default CollectionView
