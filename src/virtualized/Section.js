export class Section {
  constructor({ x, y, width, height }) {
    this.x = x;
    this.y = y;
    this.height = height;
    this.width = width;

    this.indexMap = {};
    this.indices = [];
  }

  addCellIndex({ index }) {
    if (!this.indexMap[index]) {
      this.indexMap[index] = true;
      this.indices.push(index);
    }
  }

  getCellIndices() {
    return this.indices;
  }

  toString() {
    return `${this.x},${this.y} ${this.width}x${this.height}`;
  }
}


const DEFAULT_SECTION_SIZE = 50

class SectionManager {
  constructor(sectionSize = DEFAULT_SECTION_SIZE) {
    this.sectionSize = sectionSize;

    this.cellMetadata = [];
    this.sections = [];
  }

  getCellMetadata({ index }) {
    if (index == null) return undefined
    return this.cellMetadata[index];
  }

  getSectionSize() {
    return this.sectionSize
  }

  getCellIndices({ x, y, width, height }) {
    const indices = {};
    this.getSections({ x, y, width, height }).forEach(section =>
      section.getCellIndices().forEach(index => (indices[index] = index))
    );
    return Object.keys(indices).map(index => indices[index]);
  }

  getSections({ x, y, width, height }) {
    const sectionXStart = Math.floor(x / this.sectionSize);
    const sectionXStop = Math.floor((x + width - 1) / this.sectionSize);
    const sectionYStart = Math.floor(y / this.sectionSize);
    const sectionYStop = Math.floor((y + height - 1) / this.sectionSize);

    const sections = [];

    for (let sectionX = sectionXStart; sectionX <= sectionXStop; sectionX++) {
      for (let sectionY = sectionYStart; sectionY <= sectionYStop; sectionY++) {
        const key = `${sectionX}.${sectionY}`;

        if (!this.sections[key]) {
          this.sections[key] = new Section({
            height: this.sectionSize,
            width: this.sectionSize,
            x: sectionX * this.sectionSize,
            y: sectionY * this.sectionSize,
          });
        }

        sections.push(this.sections[key]);
      }
    }

    return sections;
  }

  registerCell({index, cellMetadata}) {
    this.cellMetadata[index] = cellMetadata

    this.getSections(cellMetadata).forEach(section => section.addCellIndex({index}))
  }

  toString() {
    return Object.keys(this.sections).map(index =>
      this.sections[index].toString(),
    ).join(', ');
  }
}

export default SectionManager;
