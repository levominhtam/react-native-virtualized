import React, { PureComponent } from "react"
import { View, Text } from "react-native"

export class OverscanView extends PureComponent {
  constructor(props) {
    super(props)
  }

  render() {
    console.log('OverscanView render')
    return (
      <View
        style={{
          position: "absolute",
          top: -this.props.verticalOverscanSize,
          left: -this.props.horizontalOverscanSize,
          right: -this.props.horizontalOverscanSize,
          bottom: -this.props.verticalOverscanSize,
          backgroundColor: "lightgoldenrodyellow",
          borderColor: "moccasin",
          borderWidth: 1
        }}
        collapsable
        shouldRasterizeIOS
        renderToHardwareTextureAndroid
      >
        <Text
          style={{
            position: "absolute",
            width: "100%",
            height: this.props.verticalOverscanSize,
            textAlign: "center",
            textAlignVertical: "center",
            textTransform: "uppercase",
            letterSpacing: 4,
            opacity: 0.3
          }}
        >
          pre-load
        </Text>
        <Text
          style={{
            position: "absolute",
            width: "100%",
            height: this.props.horizontalOverscanSize,
            textAlign: "center",
            textAlignVertical: "center",
            textTransform: "uppercase",
            letterSpacing: 4,
            opacity: 0.3,
            transform: [
              {
                translateX:
                  -this.props.IMAGE_WIDTH / 2 -
                  this.props.horizontalOverscanSize / 2
              },
              {
                translateY:
                  this.props.IMAGE_HEIGHT / 2 +
                  this.props.horizontalOverscanSize / 2
              },
              {
                rotate: "-90deg"
              }
            ]
          }}
        >
          pre-load
        </Text>
      </View>
    )
  }
}
