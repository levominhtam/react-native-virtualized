/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react"
import { Platform, StyleSheet, Text, View, Dimensions, FlatList } from "react-native"
import Collection from "./virtualized/Collection"
import Cell from "./Cell"
import DebugView from './dev/DebugView'

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
})

function getRandomColor() {
  var letters = "0123456789ABCDEF"
  var color = "#"
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)]
  }
  return color
}

const TOTAL_ITEMS = 1000
const MAX_SIZE = TOTAL_ITEMS * 1.5
const WIDTH = MAX_SIZE * 3
const HEIGHT = MAX_SIZE * 3

const DEVICE_WIDTH = Dimensions.get('window').width
const DEVICE_HEIGHT = Dimensions.get('window').height

let columnX = 0
let columnY = 0

const data = Array.from(Array(TOTAL_ITEMS), (_, index) => ({
  color: getRandomColor(),
  index
}))

const DEBUG = true

type Props = {}
export default class App extends Component<Props> {

  renderItem = (item, index) => <Cell index={index} data={item} />
  renderItemFlatlist = ({item, index}) => <Cell index={index} data={item} />
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Collection (Custom)</Text>
        <Collection
          style={styles.collectionView}
          contentContainerStyle={styles.content}
          data={data}
          renderItem={this.renderItem}
          getItemLayout={cellFrameGetterAsList}
          horizontalOverscanSize={32}
          verticalOverscanSize={DEVICE_HEIGHT}
          debug={DEBUG}
        />
        
        <Text style={styles.welcome}>FlatList (React Native)</Text>
        <FlatList
          style={styles.collectionView}
          data={data}
          renderItem={this.renderItemFlatlist}
        />

        <DebugView ref={(component) => DebugView.instance = component} />
      </View>
    )
  }
}

const cellFrameGetterSequence = ({ index }) => {
  const offset = 50 * index
  const remainder = offset % MAX_SIZE
  const numberOfRow = Math.floor(offset / MAX_SIZE)
  let x = remainder
  let y = numberOfRow * 50
  const width = 50
  const height = 50
  return { x, y, width, height }
}

const cellFrameGetterRandom = ({ index }) => {
  let x = Math.random() * WIDTH
  let y = Math.random() * HEIGHT
  const width = 50
  const height = 50
  return { x, y, width, height }
}

const cellFrameGetterAsList = ({ index }) => {
  let x = 0
  let y = 50 * index
  const width = 350
  const height = 50
  return { x, y, width, height }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: "#F5FCFF",
  },
  collectionView: {
    flex: 1,
    width: 350,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: "black",
    shadowOpacity: 0.35,
    shadowRadius: 4,
  },
  content: {
    backgroundColor: "white",
    borderRadius: 10,
    elevation: 4,
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
})
