import React, { Component } from "react"
import { Text, View } from "react-native"
import Animated from "react-native-reanimated"

const { Value, multiply, block, onChange, call, cond } = Animated

export class CellRender extends Component {
  constructor(props) {
    super(props)

    this.last = {}
    this.x = new Value(0)
    this.y = new Value(0)
    this.width = new Value(0)
    this.height = new Value(0)
    
    this.lockX = new Value(0)
    this.lockY = new Value(0)
    this.lockWidth = new Value(0)
    this.lockHeight = new Value(0)

    const { context: { scaleX, scaleY } } = props
    this.left = cond(this.lockX, this.x, multiply(this.x, scaleX))
    this.top = cond(this.lockY, this.y, multiply(this.y, scaleY))
    this.w = cond(this.lockWidth, this.width, multiply(this.width, scaleX))
    this.h = cond(this.lockHeight, this.height, multiply(this.height, scaleY))

    this.updateFrame(props.cellMetadata)
    this.updateConfig(props.cellConfig)
  }

  shouldComponentUpdate(nextProps) {
    return this.props.boundTo !== nextProps.boundTo
  }

  componentWillUpdate(nextProps) {
    const { boundTo } = this.props
    if (boundTo !== nextProps.boundTo) {
      if (nextProps.boundTo == null) return
      if (nextProps.cellMetadata) this.updateFrame(nextProps.cellMetadata)
      if (nextProps.cellConfig) this.updateConfig(nextProps.cellConfig)
    }
  }

  updateFrame = ({ x, y, width, height }) => {
    this.updateValue("x", x)
    this.updateValue("y", y)
    this.updateValue("width", width)
    this.updateValue("height", height)
  }

  updateConfig = ({ lockX, lockY, lockWidth, lockHeight }) => {
    this.updateValue("lockX", lockX ? 1 : 0)
    this.updateValue("lockY", lockY ? 1 : 0)
    this.updateValue("lockWidth", lockWidth ? 1 : 0)
    this.updateValue("lockHeight", lockHeight ? 1 : 0)
  }

  updateValue(key, value) {
    if (value == null) return
    if (this.last[key] !== value) {
      this.last[key] = value
      this[key].setValue(value)
    }
  }

  render() {
    const { render, boundTo, index } = this.props

    const actived = boundTo != null
    let layoutStyle = {}
    if (this.props.cellMetadata.width) {
      layoutStyle = {
        width: this.props.cellMetadata.width,
        height: this.props.cellMetadata.height,
        transform: [{ translateX: this.props.cellMetadata.x }, { translateY: this.props.cellMetadata.y }],
      }
    }
    // console.log(`Cell ${index} ==> ${!actived ? 'null': boundTo}`);
    const style = {
      position: "absolute",
      // width: this.w,
      // height: this.h,
      // transform: [{ translateX: this.left }, { translateY: this.top }],
    }

    return (
      <Animated.View
        style={[style, layoutStyle]}
        pointerEvents={actived ? "auto" : "none"}
      >
        {actived && render(boundTo)}
      </Animated.View>
    )
  }
}

export default CellRender
