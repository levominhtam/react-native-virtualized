import React, { PureComponent } from "react"
import { Text, View, TouchableOpacity } from "react-native"
import DebugView from './dev/DebugView'

export class Cell extends PureComponent {
  state = {
    opened: true
  }
  renderAppointmentType() {
    const title = "Takami"
    const subtitle = "abc/def"
    const notes = "foo note"
    const colors = {
      background: "#fff6d4",
      stroke: "#ff9a00",
      title: "#d37700",
      subtitle: "#de9735"
    }

    const content = {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      overflow: "hidden",
      backgroundColor: colors.background,
      borderWidth: 0.5,
      borderColor: "#ff9a00",
      padding: 4,
      borderRadius: 4
    }

    return (
      <View style={content}>
        {title && (
          <Text
            ellipsizeMode={"tail"}
            numberOfLines={1}
            style={{ color: colors.title, fontSize: 12 }}
          >
            {title}
          </Text>
        )}
        {subtitle && (
          <Text
            ellipsizeMode={"tail"}
            numberOfLines={1}
            style={{ color: colors.subtitle, fontSize: 9 }}
          >
            {subtitle}
          </Text>
        )}
        <View
          style={{ flexGrow: 1, backgroundColor: "transparent", minHeight: 4 }}
        />
        {notes && (
          <Text
            ellipsizeMode={"tail"}
            numberOfLines={1}
            style={{ color: colors.subtitle, fontSize: 9 }}
          >
            {notes}
          </Text>
        )}

        <View style={{ alignItems: "flex-start" }}>
          <View
            style={{
              backgroundColor: colors.stroke,
              paddingHorizontal: 6,
              paddingVertical: 2,
              height: 16,
              borderRadius: 9
            }}
          >
            <Text
              ellipsizeMode={"tail"}
              numberOfLines={1}
              style={{ color: colors.background, fontSize: 9 }}
            >
              foo-tag
            </Text>
          </View>
        </View>
      </View>
    )
  }

  render() {
    const { data, index } = this.props
    const { color } = data
    const { opened } = this.state
    // DebugView.addLog(`Cell ${index} be render`)
    return (
      <View
        key={index}
        style={{
          flex: 1,
          minHeight: 50,
          backgroundColor: opened ? color : "gray",
          justifyContent: "center",
          overflow: "hidden"
        }}
      >
        {!opened && (
          <TouchableOpacity
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
            onPress={() => this.setState({ opened: true })}
          >
            <Text style={{ color: "white" }}> TAP ME </Text>
          </TouchableOpacity>
        )}

        {/* {opened && this.renderAppointmentType()} */}

        {opened && (
          <View style={{padding: 4, justifyContent: 'center', alignItems: 'center'}}>
          {/* {this.renderAppointmentType()} */}
          <Text
            style={{
              fontSize: 14,
              fontWeight: "bold",
              color: "white",
              textAlign: "center",
              textAlignVertical: "center",
              backgroundColor: "black",
              borderRadius: 10,
              minHeight: 20,
              minWidth: 40,
              overflow: 'hidden',
              padding: 2,
            }}
            adjustsFontSizeToFit
          >
            {index}
          </Text>
          </View>
        )}
      </View>
    )
  }
}

export default Cell
