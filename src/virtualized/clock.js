const uuidv4 = require('uuid/v4')

class Clock {
    id
    operation

    constructor(operation, id = uuidv4()) {
        this.operation = operation
        this.id = id
    }

    get isRunning() {
        return this.running
    }

    start() {
        console.log('Clock START')
        this.running = true
        this.tick()
    }

    stop() {
        this.tick()
        this.running = false
        console.log('Clock STOP')
    }

    tick = () => {
        if (!this.running) {
            return
        }
        console.log('Clock TICK')
        this.operation()

        requestAnimationFrame(this.tick)
    }
}

export default Clock