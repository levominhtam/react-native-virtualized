import React, { PureComponent } from "react"
import { View, ImageBackground } from "react-native"
import Animated from "react-native-reanimated"

export class SectionOverlay extends PureComponent {
  constructor(props) {
    super(props)
  }

  render() {
    console.log('SectionOverlay render')
    return (
      <Animated.View
        style={{
          position: "absolute",
          backgroundColor: "#87ceeb99",
          width: this.props.context.contentWidth,
          height: this.props.context.contentHeight,
          opacity: 0.5,
          transform: [
            {
              translateX: this.props.context.translationX
            },
            {
              translateY: this.props.context.translationY
            },
            {
              translateX: this.props.context.scaleTopLeftFixX
            },
            {
              translateY: this.props.context.scaleTopLeftFixY
            },
            {
              scaleY: this.props.context.scaleY,
            },
            // {
            //   scaleX: this.props.context.scaleX
            // },
            // {
            //   scaleY: this.props.context.scaleY
            // },
          ]
        }}
        collapsable
        shouldRasterizeIOS
        renderToHardwareTextureAndroid
      >
        {!this.props.imageHidden && (
          <ImageBackground
            style={{
              flex: 1
            }}
            source={require("../../assets/rect-50x50-without-filled.png")}
            resizeMode="repeat"
          />
        )}
      </Animated.View>
    )
  }
}
