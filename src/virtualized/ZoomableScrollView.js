import { SectionOverlay } from "./debug/SectionOverlay"
import { ViewportView } from "./debug/ViewportView"
import { OverscanView } from "./debug/OverscanView"
import DebugView from "../dev/DebugView"
import React, { Component, PureComponent } from "react"
import {
  Dimensions,
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  Platform
} from "react-native"
import {
  PanGestureHandler,
  State,
  PinchGestureHandler
} from "react-native-gesture-handler"
import Animated, { Easing } from "react-native-reanimated"
import _ from "lodash"
import Batchinator from "react-native/Libraries/Interaction/Batchinator"
import CellRender from "./CellRender"
const uuidv4 = require("uuid/v4")

const {
  concat,
  set,
  cond,
  and,
  neq,
  eq,
  or,
  add,
  sub,
  pow,
  min,
  max,
  debug,
  multiply,
  divide,
  greaterThan,
  greaterOrEq,
  lessThan,
  spring,
  defined,
  decay,
  timing,
  call,
  diff,
  acc,
  not,
  abs,
  block,
  startClock,
  stopClock,
  clockRunning,
  onChange,
  Value,
  Clock,
  event
} = Animated

const MIN_SCALE = 1
const MAX_SCALE = 4

const IMAGE_WIDTH = 200
const IMAGE_HEIGHT = 200

const Direction = {
  NONE: 0,
  X: 1 << 0,
  Y: 1 << 1,
  ALL: 1 << 2
}

function scaleDiff(value) {
  const tmp = new Value(1)
  const prev = new Value(1)
  return [set(tmp, divide(value, prev)), set(prev, value), tmp]
}

function dragDiff(value, updating) {
  const tmp = new Value(0)
  const prev = new Value(0)
  return cond(
    updating,
    [set(tmp, sub(value, prev)), set(prev, value), tmp],
    set(prev, 0)
  )
}

// returns linear friction coeff. When `value` is 0 coeff is 1 (no friction), then
// it grows linearly until it reaches `MAX_FRICTION` when `value` is equal
// to `MAX_VALUE`
function friction(value) {
  const MAX_FRICTION = 5
  const MAX_VALUE = 100
  return max(
    1,
    min(MAX_FRICTION, add(1, multiply(value, (MAX_FRICTION - 1) / MAX_VALUE)))
  )
}

function speed(value) {
  const clock = new Clock()
  const dt = diff(clock)
  return cond(lessThan(dt, 1), 0, multiply(1000, divide(diff(value), dt)))
}

function scaleRest(value, minScale = MIN_SCALE, maxScale = MAX_SCALE) {
  return cond(
    lessThan(value, minScale),
    minScale,
    cond(lessThan(maxScale, value), maxScale, value)
  )
}

function scaleFriction(value, rest, delta) {
  const MAX_FRICTION = 20
  const MAX_VALUE = 0.5
  const res = multiply(value, delta)
  const howFar = abs(sub(rest, value))
  const friction = max(
    1,
    min(MAX_FRICTION, add(1, multiply(howFar, (MAX_FRICTION - 1) / MAX_VALUE)))
  )
  return cond(
    lessThan(0, howFar),
    multiply(value, add(1, divide(add(delta, -1), friction))),
    res
  )
}

function runTiming(clock, value, dest, startStopClock = true) {
  const state = {
    finished: new Value(0),
    position: new Value(0),
    frameTime: new Value(0),
    time: new Value(0)
  }

  const config = {
    toValue: new Value(0),
    duration: 300,
    easing: Easing.inOut(Easing.cubic)
  }

  return [
    cond(clockRunning(clock), 0, [
      set(state.finished, 0),
      set(state.frameTime, 0),
      set(state.time, 0),
      set(state.position, value),
      set(config.toValue, dest),
      startStopClock && startClock(clock)
    ]),
    timing(clock, state, config),
    cond(state.finished, startStopClock && stopClock(clock)),
    state.position
  ]
}

function runDecay(clock, value, velocity) {
  const state = {
    finished: new Value(0),
    velocity: new Value(0),
    position: new Value(0),
    time: new Value(0)
  }

  const config = { deceleration: 0.99 }

  return [
    cond(clockRunning(clock), 0, [
      set(state.finished, 0),
      set(state.velocity, velocity),
      set(state.position, value),
      set(state.time, 0),
      startClock(clock)
    ]),
    set(state.position, value),
    decay(clock, state, config),
    cond(state.finished, stopClock(clock)),
    state.position
  ]
}

function bouncyPinch(
  value,
  gesture,
  gestureActive,
  focalX,
  displacementX,
  focalY,
  displacementY,
  minScale,
  maxScale
) {
  const clock = new Clock()

  const delta = scaleDiff(gesture)
  const rest = scaleRest(value, minScale, maxScale)
  const focalXRest = cond(
    lessThan(value, 1),
    0,
    sub(displacementX, multiply(focalX, add(-1, divide(rest, value))))
  )
  const focalYRest = cond(
    lessThan(value, 1),
    0,
    sub(displacementY, multiply(focalY, add(-1, divide(rest, value))))
  )
  const nextScale = new Value(1)

  return cond(
    [delta, gestureActive],
    [
      stopClock(clock),
      set(nextScale, scaleFriction(value, rest, delta)),
      set(
        displacementX,
        sub(displacementX, multiply(focalX, add(-1, divide(nextScale, value))))
      ),
      set(
        displacementY,
        sub(displacementY, multiply(focalY, add(-1, divide(nextScale, value))))
      ),
      nextScale
    ],
    cond(
      or(clockRunning(clock), not(eq(rest, value))),
      [
        set(displacementX, runTiming(clock, displacementX, focalXRest, false)),
        set(displacementY, runTiming(clock, displacementY, focalYRest, false)),
        runTiming(clock, value, rest)
      ],
      value
    )
  )
}

function bouncyPinchv2(
  value,
  gesture,
  gestureActive,
  focal,
  displacement,
  minScale,
  maxScale
) {
  const clock = new Clock()

  const delta = scaleDiff(gesture)
  const rest = scaleRest(value, minScale, maxScale)
  const focalRest = cond(
    lessThan(value, 1),
    0,
    sub(displacement, multiply(focal, add(-1, divide(rest, value))))
  )
  const nextScale = new Value(1)

  return cond(
    and(delta, gestureActive),
    [
      stopClock(clock),
      set(nextScale, scaleFriction(value, rest, delta)),
      set(
        displacement,
        sub(displacement, multiply(focal, add(-1, divide(nextScale, value))))
      ),
      nextScale
    ],
    cond(
      or(clockRunning(clock), not(eq(rest, value))),
      [
        set(displacement, runTiming(clock, displacement, focalRest, false)),
        runTiming(clock, value, rest)
      ],
      value
    )
  )
}

function bouncy(
  value,
  gestureDiv,
  gestureActive,
  lowerBound,
  upperBound,
  friction,
  controlDiv,
  controlActive
) {
  const timingClock = new Clock()
  const decayClock = new Clock()

  const velocity = min(speed(value), cond(controlActive, 1, 9999))

  // did value go beyond the limits (lower, upper)
  const isOutOfBounds = or(
    lessThan(value, lowerBound),
    lessThan(upperBound, value)
  )
  // position to snap to (upper or lower is beyond or the current value elsewhere)
  const rest = cond(
    lessThan(value, lowerBound),
    lowerBound,
    cond(lessThan(upperBound, value), upperBound, value)
  )
  // how much the value exceeds the bounds, this is used to calculate friction
  const outOfBounds = abs(sub(rest, value))

  // how much diff from last time
  const div = add(gestureDiv, controlDiv)

  // the updating progress is actived
  const actived = or(gestureActive, controlActive)

  return cond(
    [div, velocity, actived],
    [
      stopClock(timingClock),
      stopClock(decayClock),
      add(value, divide(div, friction(outOfBounds)))
    ],
    cond(
      or(clockRunning(timingClock), isOutOfBounds),
      [stopClock(decayClock), runTiming(timingClock, value, rest)],
      cond(
        or(clockRunning(decayClock), lessThan(5, abs(velocity))),
        runDecay(decayClock, value, velocity),
        value
      )
    )
  )
}

function scaleSelector(gesture, updating) {
  const start = new Value(1)
  const value = new Value(1)
  return cond(updating, sub(value, start), [set(start, gesture)])
}

function direction(gestureActive, spanX, spanY, simultaneous) {
  const direction = new Value(Direction.NONE)
  const directionNone = eq(direction, Direction.NONE)
  const dx = abs(spanX)
  const dy = abs(spanY)
  const allowDirection = or(greaterThan(dx, 0), greaterThan(dy, 0))
  return [
    cond(
      gestureActive,
      cond(
        and(directionNone, allowDirection),
        set(
          direction,
          cond(
            simultaneous,
            Direction.ALL,
            cond(greaterThan(dx, dy), Direction.X, Direction.Y)
          )
        )
      ),
      set(direction, Direction.NONE)
    ),
    direction
  ]
}

function moveDiff(distance, running) {
  const clock = new Clock()
  const state = {
    finished: new Value(0),
    position: new Value(0),
    frameTime: new Value(0),
    time: new Value(0)
  }

  const config = {
    toValue: new Value(0),
    duration: 350,
    easing: Easing.inOut(Easing.cubic)
  }

  const runnable = debug("--- is runnable:", neq(distance, 0))

  return block([
    cond(
      runnable,
      [
        cond(clockRunning(clock), 0, [
          set(state.finished, 0),
          set(state.frameTime, 0),
          set(state.time, 0),
          set(state.position, 0),
          set(config.toValue, distance),
          debug("running:", set(running, 1)),
          startClock(clock)
        ]),
        timing(clock, state, config),
        debug("distance:", set(distance, sub(config.toValue, state.position))),
        cond(state.finished, [debug("stopClock", stopClock(clock))])
      ],
      debug("running:", set(running, 0))
    ),
    debug("diff:", dragDiff(state.position, runnable))
  ])
}

const TAG = "ZoomableScrollView"

class ZoomableScrollView extends Component {
  pinchRef = React.createRef()
  panRef = React.createRef()
  pinchDirectorRef = React.createRef()

  static defaultProps = {
    horizontalOverscanSize: 25,
    verticalOverscanSize: 25,
    width: 0,
    height: 0,
    minScale: 1,
    maxScale: 4,
    simultaneousDirections: false
  }

  constructor(props) {
    super(props)
    const { minScale, maxScale, width, height, simultaneousDirections } = props
    const simultaneous = new Value(simultaneousDirections ? 1 : 0)

    this._lastScaleX = 1
    this._lastScaleY = 1

    // DECLARE TRANSX
    const panTransX = new Value(0)
    const panTransY = new Value(0)

    const dragX = new Value(0)
    const dragY = new Value(0)
    const panState = new Value(-1)

    this._onPanEvent = event([
      {
        nativeEvent: {
          translationX: dragX,
          translationY: dragY,
          state: panState
        }
      }
    ])

    const panActive = eq(panState, State.ACTIVE)
    const panDirection = direction(panActive, dragX, dragY, simultaneous)

    // PINCH
    const pinchScale = new Value(1)
    const pinchFocalX = new Value(0)
    const pinchFocalY = new Value(0)
    const pinchState = new Value(-1)

    this._onPinchEvent = event([
      {
        nativeEvent: {
          state: pinchState,
          scale: pinchScale,
          focalX: pinchFocalX,
          focalY: pinchFocalY
        }
      }
    ])

    this.pinchDistanceX = new Value(0)
    this.pinchDistanceY = new Value(0)
    const pinchActive = eq(pinchState, State.ACTIVE)
    const pinchDirection = direction(
      pinchActive,
      this.pinchDistanceX,
      this.pinchDistanceY,
      simultaneous
    )

    // SCALE
    const scaleX = new Value(1)
    const scaleY = new Value(1)

    const pinchActiveX = or(
      eq(pinchDirection, Direction.X),
      eq(pinchDirection, Direction.ALL)
    )
    const pinchActiveY = or(
      eq(pinchDirection, Direction.Y),
      eq(pinchDirection, Direction.ALL)
    )

    this._focalDisplacementX = new Value(0)
    const relativeFocalX = sub(
      pinchFocalX,
      add(panTransX, this._focalDisplacementX)
    )
    this._focalDisplacementY = new Value(0)
    const relativeFocalY = sub(
      pinchFocalY,
      add(panTransY, this._focalDisplacementY)
    )

    this._scaleX = set(
      scaleX,
      bouncyPinchv2(
        scaleX,
        pinchScale,
        pinchActiveX,
        relativeFocalX,
        this._focalDisplacementX,
        minScale,
        maxScale
      )
    )

    this._scaleY = set(
      scaleY,
      bouncyPinchv2(
        scaleY,
        pinchScale,
        pinchActiveY,
        relativeFocalY,
        this._focalDisplacementY,
        minScale,
        maxScale
      )
    )

    this.onScaleChangedCaller = call([this._scaleX, this._scaleY], ([x, y]) => {
      this._lastScaleX = x
      this._lastScaleY = y
      // DEBUG - START
      DebugView.setScale({ x, y })
      // DEBUG - END
    })

    // PAN
    const pinchDirectionActive = neq(pinchDirection, Direction.NONE)
    const panDirectionActive = neq(panDirection, Direction.NONE)
    const combinedDirection = cond(
      and(panDirectionActive, pinchDirectionActive),
      pinchDirection,
      panDirection
    )
    const panXActive = or(
      eq(combinedDirection, Direction.X),
      eq(combinedDirection, Direction.ALL)
    )
    const panYActive = or(
      eq(combinedDirection, Direction.Y),
      eq(combinedDirection, Direction.ALL)
    )
    const panFriction = value => friction(value)

    const contentWidth = new Value(width)
    const contentHeight = new Value(height)
    this.scaledContentWidth = multiply(contentWidth, this._scaleX)
    this.scaledContentHeight = multiply(contentHeight, this._scaleY)
    this.screenHeight = new Value(0)
    this.screenWidth = new Value(0)

    this._scaleTopLeftFixX = divide(
      multiply(contentWidth, add(this._scaleX, -1)),
      2
    )
    this._scaleTopLeftFixY = divide(
      multiply(contentHeight, add(this._scaleY, -1)),
      2
    )

    this.moveX = new Value(0)
    this.moveY = new Value(0)
    const moveActiveX = new Value(0)
    const moveActiveY = new Value(0)

    // X
    const panUpX = cond(
      lessThan(this._scaleX, 1),
      0,
      multiply(-1, this._focalDisplacementX)
    )
    const panLowX = add(
      panUpX,
      multiply(
        -1,
        sub(
          this.scaledContentWidth,
          min(this.screenWidth, this.scaledContentWidth)
        )
      )
    )

    this._panTransX = set(
      panTransX,
      bouncy(
        panTransX,
        dragDiff(dragX, panXActive),
        or(panActive, pinchActive),
        panLowX,
        panUpX,
        panFriction,
        moveDiff(this.moveX, moveActiveX),
        moveActiveX
      )
    )

    // Y
    const panUpY = cond(
      lessThan(this._scaleY, 1),
      0,
      multiply(-1, this._focalDisplacementY)
    )
    const panLowY = add(
      panUpY,
      multiply(
        -1,
        sub(
          this.scaledContentHeight,
          min(this.screenHeight, this.scaledContentHeight)
        )
      )
    )

    this._panTransY = set(
      panTransY,
      bouncy(
        panTransY,
        dragDiff(dragY, panYActive),
        or(panActive, pinchActive),
        panLowY,
        panUpY,
        panFriction,
        moveDiff(this.moveY, moveActiveY),
        moveActiveY
      )
    )

    this._translationX = add(this._panTransX, this._focalDisplacementX)
    this._translationY = add(this._panTransY, this._focalDisplacementY)
    this._context = {
      scaleX: this._scaleX,
      scaleY: this._scaleY,
      translationX: this._translationX,
      translationY: this._translationY,
      contentWidth: contentWidth,
      contentHeight: contentHeight,
      scaleTopLeftFixX: this._scaleTopLeftFixX,
      scaleTopLeftFixY: this._scaleTopLeftFixY
    }

    this.panActive = panActive
    this.pinchActive = pinchActive
    this.scrolling = or(panActive, pinchActive)
    this.onScrollingChangedCaller = call(
      [this.scrolling],
      this.onScrollingChanged
    )

    const scaled_left = multiply(-1, this._translationX)
    const scaled_top = multiply(-1, this._translationY)
    const scaled_width = multiply(this.screenWidth, 1)
    const scaled_height = multiply(this.screenHeight, 1)

    const standard_left = divide(scaled_left, this._scaleX)
    const standard_width = divide(scaled_width, this._scaleX)
    const standard_top = divide(scaled_top, this._scaleY)
    const standard_height = divide(scaled_height, this._scaleY)

    this._viewportMetric = {
      left: 0,
      top: 0,
      right: 0,
      bottom: 0
    }

    this.onViewportChangedCaller = call(
      [
        standard_left,
        standard_top,
        add(standard_left, standard_width),
        add(standard_top, standard_height)
      ],
      this.onViewportChanged
    )

    this.viewportX = standard_left
    this.viewportY = standard_top
    this.viewportWidth = standard_width
    this.viewportHeight = standard_height

    this.listeners = new Map()
    this.state = {
      scrollContext: this._context,
      addScrollListener: this.addListener,
      removeScrollListener: this.removeListener,
      removeScrollListeners: this.removeListeners,
      parent: this
    }

    this._panResponder = {
      onStartShouldSetResponder: evt => {
        console.log("onStartShouldSetResponder")
        if (evt.nativeEvent.touches.length === 2) {
          this.handlePinch(evt)
          return true
        }
        return false
      },
      onMoveShouldSetResponder: evt => {
        console.log("onMoveShouldSetResponder")
        if (evt.nativeEvent.touches.length === 2) {
          this.handlePinch(evt)
          return true
        }
        return false
      },
      onResponderGrant: evt => this.handlePinch(evt),
      onResponderMove: evt => this.handlePinch(evt),
      onResponderRelease: evt => this.handlePinchEnd(evt),
      onResponderRelease: evt => this.handlePinchEnd(evt)
    }
  }

  /**
  |--------------------------------------------------
  | PUBLIC METHODS
  |--------------------------------------------------
  */
  scrollTo = ({ x, y }) => {
    console.log(TAG, `scrollTo >>> x:${x}, y:${y}`)
    console.log(TAG, `scaled viewport:`, this._viewportMetric)
    console.log(TAG, `scaled ratio:`, this._lastScaleX, this._lastScaleY)
    const dx = (x - this._viewportMetric.left) * this._lastScaleX
    const dy = (y - this._viewportMetric.top) * this._lastScaleY
    // const dy = 50
    console.log(TAG, "scaled dx:", dx, "scaled dy:", dy)
    this.scroll({ dx, dy })
  }

  scroll = ({ dx, dy }) => {
    this.moveX.setValue(-dx)
    this.moveY.setValue(-dy)
  }

  /**
  |--------------------------------------------------
  | PUBLIC METHODS - END
  |--------------------------------------------------
  */

  handlePinch(evt) {
    const touches = evt.nativeEvent.touches
    if (touches.length === 2) {
      const touch1 = touches[0]
      const touch2 = touches[1]
      const spanX = Math.abs(touch1.pageX - touch2.pageX)
      const spanY = Math.abs(touch1.pageY - touch2.pageY)
      this.pinchDistanceX.setValue(spanX)
      this.pinchDistanceY.setValue(spanY)
    }
  }

  handlePinchEnd(evt) {
    this.pinchDistanceX.setValue(0)
    this.pinchDistanceY.setValue(0)
  }

  addListener = (name, callback) => {
    this.listeners.set(name, callback)
  }

  removeListener = name => {
    this.listeners.delete(name)
  }

  removeListeners = () => {
    this.listeners.clear()
  }

  onLayout = ({ nativeEvent }) => {
    const { height, width } = nativeEvent.layout
    if (this.height !== height || this.width !== width) {
      this.height = height
      this.width = width
      this.screenHeight.setValue(height)
      this.screenWidth.setValue(width)
      this.notifyChanges()
    }
  }

  onScrollingChanged = ([scrolling]) => {
    this._isScrolling = !!scrolling
    this.notifyChanges()
  }

  onViewportChanged = ([l, t, r, b]) => {
    const { left, top, right, bottom } = this._viewportMetric
    if (left !== l || top !== t || right !== r || bottom !== b) {
      this._viewportMetric.left = l
      this._viewportMetric.right = r
      this._viewportMetric.top = t
      this._viewportMetric.bottom = b
      // console.log(TAG, `viewport:`, this._viewportMetric)
      this.notifyChanges()
    }
  }

  notifyChanges = () => {
    for (let [key, callback] of this.listeners) {
      callback(this._viewportMetric, this._isScrolling)
    }
  }

  renderDebugPreloadAreaOverlay() {
    const { verticalOverscanSize, horizontalOverscanSize, debug } = this.props
    if (!debug) return null
    return (
      <OverscanView
        verticalOverscanSize={verticalOverscanSize}
        horizontalOverscanSize={horizontalOverscanSize}
        IMAGE_WIDTH={IMAGE_WIDTH}
        IMAGE_HEIGHT={IMAGE_HEIGHT}
      />
    )
  }

  renderDebugViewportOverlay() {
    if (!this.props.debug) return null
    return <ViewportView />
  }

  render() {
    const { style, contentContainerStyle, debug } = this.props
    const custom = debug ? { overflow: "hidden" } : {}

    return (
      <View
        style={[styles.container, style]}
        onLayout={this.onLayout}
        {...this._panResponder}
      >
        <Animated.Code>
          {() =>
            block([
              onChange(this._scaleX, this.onScaleChangedCaller),
              onChange(this._scaleY, this.onScaleChangedCaller),
              onChange(this.scrolling, this.onScrollingChangedCaller),
              onChange(this.viewportX, this.onViewportChangedCaller),
              onChange(this.viewportY, this.onViewportChangedCaller),
              onChange(this.viewportWidth, this.onViewportChangedCaller),
              onChange(this.viewportHeight, this.onViewportChangedCaller)
            ])
          }
        </Animated.Code>

        {this.renderDebugPreloadAreaOverlay()}
        {this.renderDebugViewportOverlay()}

        <PinchGestureHandler
          ref={this.pinchRef}
          simultaneousHandlers={[this.panRef, this.pinchDirectorRef]}
          onGestureEvent={this._onPinchEvent}
          onHandlerStateChange={this._onPinchEvent}
        >
          <Animated.View style={{ flex: 1 }}>
            <PanGestureHandler
              ref={this.panRef}
              avgTouches
              simultaneousHandlers={[this.pinchRef, this.pinchDirectorRef]}
              onGestureEvent={this._onPanEvent}
              onHandlerStateChange={this._onPanEvent}
            >
              <Animated.View style={[styles.content, custom, contentContainerStyle]}>
                <Animated.View
                  style={{
                    width: this.scaledContentWidth,
                    height: this.scaledContentHeight,
                    transform: [
                      { translateX: this._panTransX },
                      { translateY: this._panTransY },
                      { translateX: this._focalDisplacementX },
                      { translateY: this._focalDisplacementY }
                    ],
                  }}
                >
                  {this.props.children(this.state)}
                </Animated.View>
              </Animated.View>
            </PanGestureHandler>
          </Animated.View>
        </PinchGestureHandler>
      </View>
    )
  }
}

export default ZoomableScrollView

const styles = StyleSheet.create({
  container: {
  },
  content: {
    flex: 1,
    backgroundColor: "#F5FCFF",
  },
})
