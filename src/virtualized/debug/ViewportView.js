import React, { PureComponent } from "react"
import { View, Text } from "react-native"

export class ViewportView extends PureComponent {
  constructor(props) {
    super(props)
  }

  render() {
    console.log('ViewportView render')
    return (
      <View
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          backgroundColor: "orange",
          borderColor: "darkorange",
          borderWidth: 1,
          opacity: 0.3,
          alighItems: "center",
          justifyContent: "center"
        }}
        collapsable
        shouldRasterizeIOS
        renderToHardwareTextureAndroid
      >
        <Text
          style={{
            textAlign: "center",
            letterSpacing: 4
          }}
        >
          VIEWPORT
        </Text>
      </View>
    )
  }
}
