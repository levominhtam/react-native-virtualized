import SectionManager from './Section'

function isDefined(value) {
  return value != null && !isNaN(value)
}

export function calculateFrameData({ cellCount, cellFrameGetter, cellConfigGetter }) {
  const cellMetadata = []
  const cellConfigs = []
  let boundaryWidth = 0
  let boundaryHeight = 0
  const sectionManager = new SectionManager()
  // console.log('cellCount', cellCount);
  // console.log('cellFrameGetter', cellFrameGetter);

  for (let index = 0; index < cellCount; index++) {
    const cellMetadatum = cellFrameGetter({ index })
    const cellConfig = cellConfigGetter({ index })
    // console.log(index, 'cellMetadatum', cellMetadatum);
    const { x, y, width, height } = cellMetadatum
    if (
      !isDefined(x) ||
      !isDefined(y) ||
      !isDefined(width) ||
      !isDefined(height)
    ) {
      throw Error(
        `Invalid metadata returned for cell ${index}: x:${x}, y:${y}, width:${width}, height:${height}`
      )
    }
    boundaryHeight = Math.max(boundaryHeight, y + height)
    boundaryWidth = Math.max(boundaryWidth, x + width)
    // console.log(index, '    boundaryWidth', boundaryWidth);
    // console.log(index, '    boundaryHeight', boundaryHeight);

    cellMetadata[index] = cellMetadatum
    cellConfigs[index] = Object.assign({
      lockX: false,
      lockY: false,
      lockWidth: false,
      lockHeight: false,
    }, cellConfig)
    sectionManager.registerCell({ index, cellMetadata: cellMetadatum })
  }

  // console.log('FINAL, boundaryWidth', boundaryWidth);
  // console.log('FINAL, boundaryHeight', boundaryHeight);
  return {
    cellMetadata,
    cellConfigs,
    sectionManager,
    boundaryWidth,
    boundaryHeight
  }
}

export default calculateFrameData