import React, { PureComponent } from "react"
import CollectionView from "./CollectionView"
import SectionManager from "./Section"
import ZoomableScrollView from "./ZoomableScrollView"
import { calculateFrameData } from "./FrameHelper"
const invariant = require("invariant")

export class Collection extends PureComponent {
  hookCollectionViewRef = ref => (this.collectionViewRef = ref)

  static defaultProps = {
    cellGroupRender: defaultCellGroupRender,
    debug: false, // for debug ONLY. It's maybe affect to performance.
    getItemLayoutConfig: ({ index }) => null
  }

  constructor(props) {
    super(props)
    const { data, getItemLayout, renderItem } = props

    invariant(data, "Data is null or undefined. `data` is required.")
    invariant(
      getItemLayout,
      "Layout for item invalid. You likely forgot to implement `getItemLayout` method or return a invalid object. \n(ex: valid return look like that {x, y, width, heigth})"
    )
    invariant(
      renderItem,
      "renderItem is missing. You likely forgot to implement `renderItem` method."
    )

    this.cellMetadata = []
    this.cellCache = []
    this.cellConfigs = []
  }

  /**
 |--------------------------------------------------
 | Collection View Layout Manager delegates
 |--------------------------------------------------
 */
  prepareLayout() {
    const { data, getItemLayout, getItemLayoutConfig } = this.props

    const meta = calculateFrameData({
      cellCount: data.length,
      cellFrameGetter: ({ index }) => getItemLayout({item: data[index], index}),
      cellConfigGetter: ({ index }) =>
        getItemLayoutConfig({item: data[index], index})
    })

    this.cellMetadata = meta.cellMetadata
    this.cellConfigs = meta.cellConfigs
    this.sectionManager = meta.sectionManager
    this.boundaryWidth = meta.boundaryWidth
    this.boundaryHeight = meta.boundaryHeight
    this.width = meta.boundaryWidth
    this.height = meta.boundaryHeight
    // console.log('cellMetadata', this.cellMetadata);
    // console.log(`size: ${this.width}x${this.height}`);
    // console.log(`section manager: ${this.sectionManager.toString()}`);
  }

  getContentSize() {
    return {
      width: this.width,
      height: this.height
    }
  }

  getSectionSize() {
    return this.sectionManager.getSectionSize()
  }

  getCellIndices({ x, y, width, height }) {
    return this.sectionManager.getCellIndices({
      x,
      y,
      width,
      height
    })
  }

  getCellMetadata = ({ index }) =>
    this.sectionManager.getCellMetadata({ index })

  getCellConfig = ({ index }) => this.cellConfigs[index]

  isScrollingChanged = (scrolling) => {
    if (!scrolling) {
      this.cellCache = []
    }
  }

  cellRenderer({ indice, isScrolling, context }) {
    const { renderItem, data } = this.props
    return defaultCellRender({
      cellCache: this.cellCache,
      cellRender: renderItem,
      cellDataGetter: ({ index }) => data[index],
      cellMetadataGetter: this.getCellMetadata,
      index: indice,
      isScrolling,
      context
    })
  }

  cellRenderers({ x, y, width, height, isScrolling }) {
    const indices = this.getCellIndices({ x, y, width, height })
    return this._cellRenderersByIndices({ indices, isScrolling, context })
  }

  _cellRenderersByIndices({ indices, isScrolling, context }) {
    const { renderItem, cellGroupRender } = this.props
    this.lastRenderedCellIndices = indices
    return cellGroupRender({
      cellCache: this.cellCache,
      cellRender: renderItem,
      cellDataGetter: ({ index }) => data[index],
      cellMetadataGetter: this.getCellMetadata,
      indices: this.lastRenderedCellIndices,
      isScrolling,
      context
    })
  }

  render() {
    const { ...props } = this.props
    const { style, contentContainerStyle, ...contentProps } = props
    return (
      <ZoomableScrollView {...props}>
        {state => (
          <CollectionView
            ref={this.hookCollectionViewRef}
            layoutManager={this}
            {...contentProps}
            {...state}
          />
        )}
      </ZoomableScrollView>
    )
  }
}

function defaultCellGroupRender(args) {
  const {
    cellCache,
    cellRender,
    cellMetadataGetter,
    cellDataGetter,
    indices,
    isScrolling,
    context
  } = args
  return indices
    .map(index => defaultCellRender({ ...args, index }))
    .filter(cell => !!cell)
}

function defaultCellRender({
  cellCache,
  cellRender,
  cellMetadataGetter,
  cellDataGetter,
  index,
  isScrolling,
  context
}) {
  const cellMetadata = cellMetadataGetter({ index })
  const cellData = cellDataGetter({ index })
  const cellProps = {
    index,
    key: index,
    isScrolling,
    metadata: cellMetadata
  }

  if (isScrolling) {
    if (!cellCache.includes(index)) {
      cellCache[index] = cellRender(cellData, index, cellProps)
    }
    return cellCache[index]
  } else {
    return cellRender(cellData, index, cellProps)
  }
}

export default Collection
