/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import Collection from "./virtualized/Collection";
import Cell from "./Cell";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

function getRandomColor() {
  var letters = "0123456789ABCDEF";
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

const TOTAL_ITEMS = 300
const MAX_SIZE = TOTAL_ITEMS * 1.5;
const WIDTH = MAX_SIZE * 3;
const HEIGHT = MAX_SIZE * 3;

let columnX = 0;
let columnY = 0;

type Props = {};
export default class App extends Component<Props> {
  constructor(props: Props) {
    super(props);
    this.data = Array.from(Array(TOTAL_ITEMS), (_, index) => ({
      color: '#fff6d4', //getRandomColor(),
      index
    }));
  }

  render() {
    return (
      <View style={styles.container}>
        <Collection
          style={{
            backgroundColor: 'white',
            borderRadius: 4,
            shadowOffset: { width: 0, height: 0 },
            shadowColor: 'black',
            shadowOpacity: 0.35,
            shadowRadius: 4,
            elevation: 4,
          }}
          data={this.data}
          renderItem={(item, index) => <Cell index={index} data={item} />}
          getItemLayout={cellFrameGetterRandom}
          debug={true}
        />
      </View>
    );
  }
}

const cellFrameGetterSequence = ({ index }) => {
  const offset = 50 * index;
  const remainder = offset % MAX_SIZE;
  const numberOfRow = Math.floor(offset / MAX_SIZE);
  let x = remainder;
  let y = numberOfRow * 50;
  const width = 50;
  const height = 50;
  return { x, y, width, height };
};

const cellFrameGetterRandom = ({ index }) => {
  let x = Math.random() * WIDTH;
  let y = Math.random() * HEIGHT;
  const width = 50 + Math.random() * 25;
  const height = 50 + Math.random() * 25;
  return { x, y, width, height };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
